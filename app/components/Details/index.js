import React from 'react';

export default class Details extends React.PureComponent {
  render() {
    const { address, contact, general, job } = this.props.picked;
    return (
      <div>
        <div>
          <img src={general.avatar} />
          <div>
            <span>{general.firstName}</span>
            <span>{general.lastName}</span>
          </div>
        </div>
        <div>
          <span>{address.country}</span>
          <span>{address.city}</span>
          <span>{address.street}</span>
          <span>{address.zipCode}</span>
        </div>
        <div>
          <span>{contact.email}</span>
          <span>{contact.phone}</span>
        </div>
        <div>
          <span>{job.company}</span>
          <span>{job.title}</span>
        </div>
      </div>
    );
  }
}
