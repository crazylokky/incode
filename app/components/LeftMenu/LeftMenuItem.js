import React from 'react';
import { Image } from 'semantic-ui-react';

import './listItemStyle.css';

export default class LeftMenuItem extends React.PureComponent {

  handlePickClient = () => {
    this.props.pickClient(this.props.index);
  };

  render() {
    const { general } = this.props.client;
    return (
      <div onClick={this.handlePickClient} className="list-item">
        <Image src={general.avatar} size="small" />
        <div>
          <span>{general.firstName}</span><br />
          <span>{general.lastName}</span>
        </div>
      </div>
    );
  }
}
