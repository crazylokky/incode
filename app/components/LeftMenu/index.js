import React from 'react';
import flattenDeep from 'lodash.flattendeep';
import 'semantic-ui-css/semantic.min.css';
import LeftMenuItem from './LeftMenuItem';
import './style.css';
import Search from '../Search';

export default class LeftMenu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      filter: '',
    };
  }

  handleFilter = (f) => {
    this.setState({ filter: f });
  };

  filterClients = (value) => {
    return this.flattenClients.map((client) => client.filter((item) => item.includes(value)).length > 0);
  };

  get flattenClients() {
    return this.props.clients.map((client) => Object.keys(client).map((key) => Object.keys(client[key]).map((k) => client[key][k]))).map((item) => flattenDeep(item));
  }

  render() {
    const filtered = this.filterClients(this.state.filter);
    return (
      <div className="left-side-menu">
        <Search handleFilter={this.handleFilter} />
        {
          this.props.clients.map((client, index) => (
            filtered[index] &&
            <LeftMenuItem key={index} client={client} index={index} pickClient={this.props.pickClient} />
          ))
        }
      </div>
    );
  }
}
