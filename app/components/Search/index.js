import React from 'react';
import { Input } from 'semantic-ui-react';

export default class Search extends React.PureComponent {
  handleChange = (e) => {
    this.props.handleFilter(e.target.value);
  };
  render() {
    return (
      <div>
        <Input onChange={this.handleChange} icon="search" placeholder="Search..." />
      </div>
    );
  }
}
