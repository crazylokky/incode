import * as actionTypes from './constants';

export function loadClientsSaga() {
  return {
    type: actionTypes.LOAD_DATA,
  };
}

export function loadClients(data) {
  return {
    type: actionTypes.LOAD_SUCCESS,
    data,
  };
}

export function loadError(e) {
  return {
    type: actionTypes.LOAD_ERROR,
    e,
  };
}

export function pickClient(index) {
  return {
    type: actionTypes.PICK_CLIENT,
    index,
  };
}
