import React from 'react';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import PropTypes from 'prop-types';
import injectSaga from 'utils/injectSaga';

import Details from '../../components/Details';
import LeftMenu from '../../components/LeftMenu';
import {
  loadClientsSaga,
  pickClient,
} from './actions';
import saga from './saga';

import reducer from './reducer';
import { makeSelectorPayload, makeSelectorPickedClient } from './selectors';
import './style.css';


export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    loadClientsSaga: PropTypes.func,
  };

  componentDidMount() {
    this.props.loadClientsSaga();
  }

  render() {
    return (
      <div className="container">
        <LeftMenu clients={this.props.clients} pickClient={this.props.pickClient} />
        {this.props.picked !== -1 && <Details picked={this.props.clients[this.props.picked]} />}
      </div>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    loadClientsSaga,
    pickClient,
  }, dispatch);
}

const mapStateToProps = createStructuredSelector({
  clients: makeSelectorPayload(),
  picked: makeSelectorPickedClient(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'clients', reducer });
const withSaga = injectSaga({ key: 'clients', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);

