import { fromJS } from 'immutable';

import * as actionTypes from './constants';

const initialState = fromJS({
  payload: [],
  pickedDetails: -1,
  error: '',
});

function clientsReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.LOAD_SUCCESS:
      return state.set('payload', action.data);
    case actionTypes.LOAD_ERROR:
      return state.set('error', action.e);
    case actionTypes.PICK_CLIENT:
      return state.set('pickedDetails', action.index);
    default:
      return state;
  }
}

export default clientsReducer;
