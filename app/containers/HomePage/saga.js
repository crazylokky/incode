import { put, takeEvery } from 'redux-saga/effects';
import { loadClients, loadError } from './actions';
import json from './clients.json';
import { LOAD_DATA } from './constants';

export function* loadClientsSaga() {
  try {
    yield put(loadClients(json));
  } catch (e) {
    yield put(loadError(e));
  }
}

export default function* initSaga() {
  yield takeEvery(LOAD_DATA, loadClientsSaga);
}
