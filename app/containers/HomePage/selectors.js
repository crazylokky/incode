import { createSelector } from 'reselect';

const selectClients = (state) => state.get('clients');

const makeSelectorPayload = () => createSelector(
  selectClients,
  (clientState) => clientState.get('payload')
);

const makeSelectorPickedClient = () => createSelector(
  selectClients,
  (clientState) => clientState.get('pickedDetails')
);

export {
  makeSelectorPayload,
  makeSelectorPickedClient,
  selectClients,
};
